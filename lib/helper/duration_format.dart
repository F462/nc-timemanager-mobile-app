extension DurationFormat on Duration {
  String toFormattedString(
      {bool showSeconds: false, showDays: false, joiner = ' '}) {
    var minutes = inMinutes;
    var days = 0;
    if (showDays) {
      days = minutes ~/ Duration.minutesPerDay;
      minutes -= days * Duration.minutesPerDay;
    }
    final hours = minutes ~/ Duration.minutesPerHour;
    minutes -= hours * Duration.minutesPerHour;

    final List<String> tokens = [];
    if (days != 0) {
      tokens.add('${days}d');
    }
    if (tokens.isNotEmpty || hours != 0) {
      tokens.add('${hours}h');
    }
    tokens.add('${minutes}m');
    if (showSeconds) {
      tokens.add('${inSeconds % 60}s');
    }

    return tokens.join(joiner);
  }
}

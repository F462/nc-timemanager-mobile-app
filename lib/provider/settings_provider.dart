import 'dart:async';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../provider/nextcloud_auth_provider.dart';
import '../provider/theme_provider.dart';

enum ThemeStyle { System, Dark, Light, Amoled }
enum StartView { Dashboard, Clients, Projekts, Tasks, TimeEntries }
enum RecordingType { Duration, TimeFromTo }

class SettingsProvider with ChangeNotifier {
  final _storage = FlutterSecureStorage();
  var _loaded = false;

  ThemeStyle _themeStyle = ThemeStyle.System;
  Color _customAccentColor = Colors.white;
  bool _useCustomAccentColor = true;
  Duration _defaultDuration = Duration(minutes: 60);
  StartView _startView = StartView.Clients;
  int _startOfWeek = DateTime.monday;
  bool _showDays = false;
  RecordingType _recordingType = RecordingType.Duration;

  ThemeStyle get themeStyle => _themeStyle;

  set themeStyle(ThemeStyle value) {
    _themeStyle = value;
    notifyListeners();
    _storage.write(key: 'themeStyle', value: _themeStyle.index.toString());
  }

  Color get customAccentColor => _customAccentColor;

  set customAccentColor(Color value) {
    _customAccentColor = value;
    notifyListeners();
    _storage.write(
      key: 'customAccentColor',
      value: _customAccentColor.value.toString(),
    );
  }

  bool get useCustomAccentColor => _useCustomAccentColor;

  set useCustomAccentColor(bool value) {
    _useCustomAccentColor = value;
    notifyListeners();
    _storage.write(
        key: 'useCustomAccentColor', value: _useCustomAccentColor.toString());
  }

  Duration get defaultDuration => _defaultDuration;

  set defaultDuration(Duration value) {
    _defaultDuration = value;
    notifyListeners();
    _storage.write(
        key: 'defaultDuration', value: _defaultDuration.inMinutes.toString());
  }

  StartView get startView => _startView;

  set startView(StartView startView) {
    _startView = startView;
    notifyListeners();
    _storage.write(key: 'startView', value: startView.index.toString());
  }

  int get startOfWeek => _startOfWeek;

  set startOfWeek(int startView) {
    _startOfWeek = startOfWeek;
    notifyListeners();
    _storage.write(key: 'startOfWeek', value: startOfWeek.toString());
  }

  bool get showDays => _showDays;

  set showDays(bool value) {
    _showDays = value;
    notifyListeners();
    _storage.write(key: 'showDays', value: _showDays.toString());
  }

  RecordingType get recordingType => _recordingType;

  set recordingType(RecordingType value) {
    _recordingType = value;
    notifyListeners();
    _storage.write(
        key: 'recordingType', value: _recordingType.index.toString());
  }

  Future<void> loadFromStorage(
    NextcloudAuthProvider webAuth,
    ThemeProvider themeProvider,
  ) async {
    if (_loaded) return;
    _loaded = true;
    final futures = await Future.wait<String?>([
      webAuth.autoLogin(), // 0
      _storage.read(key: 'themeStyle'), // 1
      _storage.read(key: 'customAccentColor'), // 2
      _storage.read(key: 'useCustomAccentColor'), // 3
      _storage.read(key: 'defaultDuration'), // 4
      _storage.read(key: 'startView'), // 5
      _storage.read(key: 'startOfWeek'), // 6
      _storage.read(key: 'showDays'), // 7
      _storage.read(key: 'recordingType'), // 8
    ]);
    if (webAuth.isAuthenticated) {
      themeProvider.update();
    }
    // themeStyle
    final ts = futures[1];
    if (ts != null) {
      _themeStyle = ThemeStyle.values[int.parse(ts)];
    }
    // customAccentColor
    final cas = futures[2];
    if (cas != null) {
      _customAccentColor = Color(int.parse(cas));
    }
    // useCustomAccentColor
    _useCustomAccentColor = futures[3] == 'true';
    // defaultDuration
    final defDur = futures[4];
    if (defDur != null) {
      _defaultDuration = Duration(minutes: int.parse(defDur));
    }
    // startView
    final sv = futures[5];
    if (sv != null) {
      _startView = StartView.values[int.parse(sv)];
    }
    // startOfWeek
    final sw = futures[6];
    if (sw != null) {
      _startOfWeek = int.parse(sw);
    }
    // showDays
    _showDays = futures[7] == 'true';
    // recordingType
    final rt = futures[8];
    print(rt);
    if (rt != null) {
      _recordingType = RecordingType.values[int.parse(rt)];
    }
  }
}

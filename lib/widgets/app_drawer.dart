import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../helper/i18n_helper.dart';
import '../provider/nextcloud_auth_provider.dart';
import '../provider/task_time_tracker_provider.dart';
import '../screens/client_overview_screen.dart';
import '../screens/dashboard_screen.dart';
import '../screens/project_overview_screen.dart';
import '../screens/settings_screen.dart';
import '../screens/task_overview_screen.dart';
import '../screens/task_time_tracker_screen.dart';
import '../screens/time_entries_screen.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer();

  @override
  Widget build(BuildContext context) {
    var isLocal = false;
    final taskTimeTracker = Provider.of<TaskTimeTrackerProvider>(context);
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            AppBar(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/launcher/icon_full.png',
                    color: Theme.of(context).accentColor,
                    colorBlendMode: BlendMode.modulate,
                    fit: BoxFit.contain,
                    height: 50,
                  ),
                  Container(
                      padding: const EdgeInsets.fromLTRB(17, 0, 0, 0),
                      child: Text(
                        'Timemanager',
                        style: TextStyle(fontSize: 20),
                      ))
                ],
              ),
              automaticallyImplyLeading: false,
              toolbarHeight: 100,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.cloud_queue_sharp,
                        color: isLocal ? Colors.red : Color(0xFF00b0ff),
                        size: 25,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      FittedBox(
                        child: Text(
                          Provider.of<NextcloudAuthProvider>(
                            context,
                            listen: false,
                          ).server!,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.person,
                        size: 25,
                        color: Color(0xFF63dd15),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      FittedBox(
                        child: Text(
                          Provider.of<NextcloudAuthProvider>(
                            context,
                            listen: false,
                          ).user!,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 2,
              color: Theme.of(context).accentColor.withAlpha(50),
            ),
            SizedBox(
              height: 10,
            ),
            ListTile(
              leading: Icon(
                Icons.dashboard_outlined,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('general.dashboard'.tl(context)),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(DashboardScreen.routeName),
            ),
            ListTile(
              leading: Icon(
                Icons.supervisor_account,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('general.clients'.tl(context)),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(ClientOverviewScreen.routeName),
            ),
            ListTile(
              leading: Icon(
                Icons.article_outlined,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('general.projects'.tl(context)),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(ProjectOverviewScreen.routeName),
            ),
            ListTile(
              leading: Icon(
                Icons.assignment_turned_in_outlined,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('general.tasks'.tl(context)),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(TaskOverviewScreen.routeName),
            ),
            ListTile(
              leading: Icon(
                Icons.access_time,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('general.time_entries'.tl(context)),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(TimeEntriesScreen.routeName),
            ),
            ListTile(
              leading: taskTimeTracker.isActive
                  ? Icon(
                      Icons.fiber_manual_record,
                      size: 30,
                      color: Colors.red,
                    )
                  : Icon(
                      Icons.fiber_manual_record_outlined,
                      size: 25,
                      color: Theme.of(context).accentColor,
                    ),
              title: Text('general.time_tracker'.tl(context)),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(TaskTimeTrackerScreen.routeName),
            ),
            ListTile(
              leading: Icon(
                Icons.settings_sharp,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('general.settings'.tl(context)),
              onTap: () =>
                  Navigator.of(context).pushNamed(SettingsScreen.routeName),
            ),
            ListTile(
              leading: Icon(
                Icons.exit_to_app_sharp,
                size: 25,
                color: Theme.of(context).accentColor,
              ),
              title: Text('app_drawer.logout'.tl(context)),
              onTap: () => logout(context),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> logout(BuildContext context) async {
    final doLogout = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('dialog.are_you_sure'.tl(context)),
        content: Text('dialog.want_logout'.tl(context)),
        actions: [
          TextButton(
            child: Text(MaterialLocalizations.of(context).cancelButtonLabel),
            onPressed: () => Navigator.of(context).pop(false),
          ),
          TextButton(
            child: Text(MaterialLocalizations.of(context).okButtonLabel),
            onPressed: () => Navigator.of(context).pop(true),
          ),
        ],
      ),
    );
    if (doLogout != null && doLogout) {
      Navigator.of(context).pop();
      Provider.of<NextcloudAuthProvider>(
        context,
        listen: false,
      ).flushLogin();
      Navigator.of(context).pushReplacementNamed('/');
    }
  }
}

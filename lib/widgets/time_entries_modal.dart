import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import './time_list_item.dart';
import '../helper/duration_format.dart';
import '../helper/i18n_helper.dart';
import '../helper/time_update.dart';
import '../model/task.dart';
import '../model/time.dart';
import '../provider/settings_provider.dart';
import '../provider/task_time_tracker_provider.dart';
import '../provider/timemanager_provider.dart';
import '../screens/task_time_tracker_screen.dart';

class TimeEntriesModal extends StatefulWidget {
  final String taskId;

  TimeEntriesModal(this.taskId);

  @override
  _TimeEntriesModalState createState() => _TimeEntriesModalState();
}

class _TimeEntriesModalState extends State<TimeEntriesModal> {
  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    final tracker = Provider.of<TaskTimeTrackerProvider>(context);
    Task task = tm.get<Task>(widget.taskId)!;
    List<Time> times = task.timesOrdered.reversed.toList();
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.top + 10),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width - 120),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        task.client.name +
                            ' - ' +
                            task.project.name +
                            ' - ' +
                            task.name,
                        softWrap: true,
                        textScaleFactor: 1.2,
                      ),
                      Text(
                        task.duration.toFormattedString(
                          showDays: Provider.of<SettingsProvider>(
                            context,
                            listen: false,
                          ).showDays,
                        ),
                      ),
                    ],
                  ),
                ),
                if (!tracker.isActive)
                  IconButton(
                    icon: Icon(
                      Icons.fiber_manual_record_outlined,
                      size: 30,
                    ),
                    onPressed: () async {
                      await _createTaskTimeTracker(task);
                    },
                  ),
                if (tracker.isActive && tracker.currentTask == task.uuid)
                  IconButton(
                    icon: Icon(
                      Icons.fiber_manual_record,
                      size: 30,
                      color: Colors.red,
                    ),
                    onPressed: () async {
                      await _endTaskTimeTracker();
                    },
                  ),
                IconButton(
                  icon: Icon(
                    Icons.more_time,
                    size: 30,
                  ),
                  onPressed: () async {
                    await updateTime(context, task);
                  },
                ),
              ],
            ),
            Card(
              child: Container(
                height: MediaQuery.of(context).size.height / 3,
                child: Scrollbar(
                  child: ListView.builder(
                    itemCount: times.length,
                    itemBuilder: (context, i) =>
                        TimeListItem(times[i], updateTime),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _createTaskTimeTracker(Task task) async {
    final tracker =
        Provider.of<TaskTimeTrackerProvider>(context, listen: false);
    tracker.start(task.uuid,
        note: 'general.tracked'.tl(context) +
            ' ' +
            DateFormat.yMd(Localizations.localeOf(context).languageCode)
                .format(DateTime.now()));
    Navigator.of(context).pushReplacementNamed(TaskTimeTrackerScreen.routeName);
  }

  Future<void> _endTaskTimeTracker() async {
    Navigator.of(context).pushReplacementNamed(TaskTimeTrackerScreen.routeName);
  }
}

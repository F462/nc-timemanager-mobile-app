import 'package:flutter/material.dart';

import '../helper/i18n_helper.dart';

class SearchGrid extends StatefulWidget {
  final int childCount;
  final Widget Function(BuildContext, int) builder;
  final void Function(String) onSearchChanged;
  final Future<void> Function() onRefresh;

  SearchGrid(
      {required this.childCount,
      required this.builder,
      required this.onSearchChanged,
      required this.onRefresh});

  @override
  _SearchGridState createState() => _SearchGridState();
}

class _SearchGridState extends State<SearchGrid> {
  final _searchTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Column(
        children: [
          Expanded(
            child: RefreshIndicator(
              onRefresh: widget.onRefresh,
              child: Scrollbar(
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverAppBar(
                      backgroundColor: Theme.of(context).canvasColor,
                      automaticallyImplyLeading: false,
                      flexibleSpace: Container(
                        padding: EdgeInsets.fromLTRB(13, 3, 13, 0),
                        child: TextField(
                          autofocus: false,
                          autocorrect: false,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintText: 'general.search_hint'.tl(context),
                            filled: false,
                            suffixIcon: IconButton(
                              alignment: Alignment.centerRight,
                              onPressed: () {
                                _searchTextController.clear();
                              },
                              icon: Icon(Icons.clear),
                              color: Theme.of(context).accentColor,
                            ),
                          ),
                          maxLines: 1,
                          controller: _searchTextController,
                          keyboardType: TextInputType.text,
                          onChanged: widget.onSearchChanged,
                        ),
                      ),
                      shadowColor: Theme.of(context).accentColor,
                      floating: true,
                      expandedHeight: 30,
                      forceElevated: true,
                    ),
                    if (widget.childCount != 0)
                      SliverGrid(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount:
                              (MediaQuery.of(context).size.width / 500).ceil(),
                          childAspectRatio: 2.2,
                          mainAxisSpacing: 5,
                          crossAxisSpacing: 5,
                        ),
                        delegate: SliverChildBuilderDelegate(
                          widget.builder,
                          childCount: widget.childCount,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

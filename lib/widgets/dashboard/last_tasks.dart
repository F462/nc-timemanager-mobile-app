import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../helper/i18n_helper.dart';
import '../../model/task.dart';
import '../../model/time.dart';
import '../../provider/task_time_tracker_provider.dart';
import '../../provider/timemanager_provider.dart';
import '../../widgets/grid_items/task_grid_item.dart';

class LastTasks extends StatelessWidget {
  final int amount;

  LastTasks(this.amount);

  @override
  Widget build(BuildContext context) {
    final tm = Provider.of<TimemanagerProvider>(context);
    final times = tm.getAll<Time>().toList();
    final taskTimeTracker = Provider.of<TaskTimeTrackerProvider>(context);
    times.sort((one, other) => other.end.compareTo(one.end));
    List<Task> tasks = [];
    var count = 0;
    var i = 0;
    if (taskTimeTracker.isActive) {
      tasks.add(tm.get<Task>(taskTimeTracker.currentTask!)!);
    }
    while (count < amount) {
      if (times.length <= i) {
        count = amount;
        continue;
      }
      if (tasks.contains(times[i].task)) {
        i++;
      } else {
        tasks.add(times[i].task);
        i++;
        count++;
      }
    }

    final min = 190.0;
    var max = 190.0;
    if (MediaQuery.of(context).size.height / 3 > max) {
      max = MediaQuery.of(context).size.height / 3;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Column(
        children: [
          Text(
            'dashboard.last_tasks'.tl(context),
            textScaleFactor: 1.3,
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: min,
              maxHeight: max,
            ),
            child: tasks.length == 0
                ? Container()
                : GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount:
                          (MediaQuery.of(context).size.width / 500).ceil(),
                      childAspectRatio: 2.2,
                      mainAxisSpacing: 5,
                      crossAxisSpacing: 5,
                    ),
                    children: tasks
                        .map((task) => TaskGridItem(
                              task.uuid,
                              null,
                              isRoot: true,
                            ))
                        .toList(),
                  ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../helper/i18n_helper.dart';
import '../../provider/task_time_tracker_provider.dart';
import '../../screens/client_overview_screen.dart';
import '../../screens/project_overview_screen.dart';
import '../../screens/task_overview_screen.dart';
import '../../screens/task_time_tracker_screen.dart';

class GoToButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Column(
        children: [
          Row(
            children: [
              TextButton.icon(
                icon: Icon(
                  Icons.supervisor_account,
                  size: 25,
                  color: Theme.of(context).accentColor,
                ),
                label: Text('general.clients'.tl(context)),
                onPressed: () => Navigator.of(context)
                    .pushReplacementNamed(ClientOverviewScreen.routeName),
              ),
              TextButton.icon(
                icon: Icon(
                  Icons.article_outlined,
                  size: 25,
                  color: Theme.of(context).accentColor,
                ),
                label: Text('general.projects'.tl(context)),
                onPressed: () => Navigator.of(context)
                    .pushReplacementNamed(ProjectOverviewScreen.routeName),
              ),
              TextButton.icon(
                icon: Icon(
                  Icons.assignment_turned_in_outlined,
                  size: 25,
                  color: Theme.of(context).accentColor,
                ),
                label: Text('general.tasks'.tl(context)),
                onPressed: () => Navigator.of(context)
                    .pushReplacementNamed(TaskOverviewScreen.routeName),
              ),
            ],
          ),
          if (Provider.of<TaskTimeTrackerProvider>(context, listen: false)
              .isActive)
            TextButton.icon(
              icon: Icon(
                Icons.fiber_manual_record,
                size: 30,
                color: Colors.red,
              ),
              label: Text('general.time_tracker'.tl(context)),
              onPressed: () => Navigator.of(context)
                  .pushReplacementNamed(TaskTimeTrackerScreen.routeName),
            ),
        ],
      ),
    );
  }
}

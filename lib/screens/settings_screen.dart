import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:provider/provider.dart';

import '../helper/duration_format.dart';
import '../helper/i18n_helper.dart';
import '../provider/settings_provider.dart';
import '../widgets/app_drawer.dart';

class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  Color _pickerColor = Color(0xFF252525);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _startViewValues = {
      StartView.Dashboard: [
        'general.dashboard'.tl(context),
        Icons.dashboard_outlined,
      ],
      StartView.Clients: [
        'general.clients'.tl(context),
        Icons.supervisor_account
      ],
      StartView.Projekts: [
        "general.projects".tl(context),
        Icons.article_outlined
      ],
      StartView.Tasks: [
        "general.tasks".tl(context),
        Icons.assignment_turned_in_outlined
      ],
      StartView.TimeEntries: [
        "general.time_entries".tl(context),
        Icons.access_time
      ],
    };
    final _startViewMenuItems = _startViewValues.keys
        .map(
          (key) => DropdownMenuItem(
            value: key,
            child: Row(
              children: [
                Icon(
                  _startViewValues[key]![1] as IconData,
                  color: Theme.of(context).accentColor,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(_startViewValues[key]![0] as String),
              ],
            ),
          ),
        )
        .toList();

    final themeStyles = {
      ThemeStyle.System: [
        'settings.system'.tl(context),
        Icons.format_paint_sharp,
      ],
      ThemeStyle.Light: [
        'settings.light_theme'.tl(context),
        Icons.format_paint_sharp,
      ],
      ThemeStyle.Dark: [
        'settings.dark_theme'.tl(context),
        Icons.format_paint_sharp,
      ],
      ThemeStyle.Amoled: [
        'settings.amoled'.tl(context),
        Icons.format_paint_sharp,
      ],
    };
    final themeStylesMenuItems = themeStyles.keys
        .map(
          (key) => DropdownMenuItem(
            value: key,
            child: Row(
              children: [
                Icon(
                  themeStyles[key]![1] as IconData,
                  color: Theme.of(context).accentColor,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(themeStyles[key]![0] as String),
              ],
            ),
          ),
        )
        .toList();

    final recordingTypes = {
      RecordingType.Duration: [
        'settings.duration'.tl(context),
        Icons.timelapse,
      ],
      RecordingType.TimeFromTo: [
        'settings.time_from_to'.tl(context),
        Icons.access_time,
      ],
    };

    final recordingTypesMenuItems = recordingTypes.keys
        .map(
          (key) => DropdownMenuItem(
            value: key,
            child: Row(
              children: [
                Icon(
                  recordingTypes[key]![1] as IconData,
                  color: Theme.of(context).accentColor,
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(recordingTypes[key]![0] as String),
              ],
            ),
          ),
        )
        .toList();

    return Scaffold(
      appBar: AppBar(
        title: Text('general.settings'.tl(context)),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      drawer: AppDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'settings.start_view'.tl(context),
                    style: TextStyle(fontSize: 16),
                  ),
                  Consumer<SettingsProvider>(
                    builder: (context, settings, child) => DropdownButton(
                      value: settings.startView,
                      onChanged: (value) {
                        settings.startView = value as StartView;
                      },
                      items: _startViewMenuItems,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  'settings.recording_type'.tl(context),
                  style: TextStyle(fontSize: 16),
                ),
                Consumer<SettingsProvider>(
                  builder: (context, settings, child) => DropdownButton(
                    value: settings.recordingType,
                    onChanged: (value) {
                      settings.recordingType = value as RecordingType;
                    },
                    items: recordingTypesMenuItems,
                  ),
                ),
              ]),
              const SizedBox(
                height: 10,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  'settings.default_duration'.tl(context),
                  style: TextStyle(fontSize: 16),
                ),
                Consumer<SettingsProvider>(
                  builder: (context, settings, child) => TextButton.icon(
                    icon: Icon(
                      Icons.timelapse,
                      color: Theme.of(context).accentColor,
                    ),
                    onPressed: _selectDuration,
                    label: Text(
                      settings.defaultDuration
                          .toFormattedString(showDays: settings.showDays),
                      textScaleFactor: 1.2,
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                  ),
                ),
              ]),
              const SizedBox(
                height: 10,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  'settings.show_days'.tl(context),
                  style: TextStyle(fontSize: 16),
                ),
                Consumer<SettingsProvider>(
                  builder: (context, settings, child) => Checkbox(
                    value: settings.showDays,
                    activeColor: Theme.of(context).accentColor,
                    onChanged: (value) => settings.showDays = value!,
                  ),
                ),
              ]),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'settings.theme'.tl(context),
                    style: TextStyle(fontSize: 16),
                  ),
                  Consumer<SettingsProvider>(
                    builder: (context, settings, child) => DropdownButton(
                      value: settings.themeStyle,
                      onChanged: (value) {
                        settings.themeStyle = value as ThemeStyle;
                      },
                      items: themeStylesMenuItems,
                    ),
                  ),
                ],
              ),
              if (Provider.of<SettingsProvider>(context).themeStyle !=
                  ThemeStyle.System)
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        'settings.accent_color'.tl(context),
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Spacer(
                      flex: 2,
                    ),
                    Consumer<SettingsProvider>(
                        builder: (context, settings, child) => Container(
                              height: 35,
                              width: 35,
                              decoration: BoxDecoration(
                                border:
                                    Border.all(width: 2, color: Colors.white),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                              child: TextButton(
                                child: Container(
                                  color: Theme.of(context).accentColor,
                                ),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: Text(
                                        'settings.select_color'.tl(context),
                                      ),
                                      content: SingleChildScrollView(
                                        child: MaterialPicker(
                                          pickerColor: _pickerColor,
                                          onColorChanged: (color) => setState(
                                            () => _pickerColor = color,
                                          ),
                                        ),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          child: Text(
                                            MaterialLocalizations.of(context)
                                                .okButtonLabel,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              settings.customAccentColor =
                                                  _pickerColor;
                                              settings.useCustomAccentColor =
                                                  true;
                                            });
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            )),
                    Spacer(),
                    Consumer<SettingsProvider>(
                      builder: (context, settings, child) => Checkbox(
                        value: settings.useCustomAccentColor,
                        activeColor: Theme.of(context).accentColor,
                        onChanged: (value) =>
                            settings.useCustomAccentColor = value!,
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _selectDuration() async {
    final settings = Provider.of<SettingsProvider>(context, listen: false);
    final result = await showTimePicker(
      context: context,
      helpText: 'general.select_duration'.tl(context),
      initialTime: TimeOfDay(
          hour: settings.defaultDuration.inHours,
          minute: settings.defaultDuration.inMinutes % 60),
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!,
        );
      },
    );
    if (result != null) {
      settings.defaultDuration =
          Duration(hours: result.hour, minutes: result.minute);
    }
  }
}

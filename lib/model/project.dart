import 'abstract_timemanager_object.dart';
import 'client.dart';
import 'task.dart';
import 'time.dart';
import 'timemanager.dart';

class Project extends AbstractTimemanagerObject {
  final String client_uuid;
  final String? color;
  final String name;
  final String? note;

  Project(
    Timemanager timemanager, {
    String? uuid,
    String? commit,
    DateTime? created,
    DateTime? changed,
    required this.client_uuid,
    this.color,
    required this.name,
    this.note,
  })  : assert(name.isNotEmpty),
        assert(client_uuid.isNotEmpty),
        super(
          timemanager,
          uuid: uuid,
          commit: commit,
          created: created,
          changed: changed,
        );

  Project.fromMap(Timemanager timemanager, Map<String, dynamic> jsonMap)
      : this(
          timemanager,
          uuid: jsonMap['uuid'],
          commit: jsonMap['commit'],
          created: DateTime.parse(jsonMap['created']),
          changed: DateTime.parse(jsonMap['changed']),
          client_uuid: jsonMap['client_uuid'],
          color: jsonMap['color'],
          name: jsonMap['name'],
          note: jsonMap['note'],
        );

  Project.copy(
    CopyBehaviour copyBehaviour,
    Timemanager timemanager,
    Project project, {
    required String client_uuid,
    String? color,
    String? name,
    String? note,
  }) : this(
          timemanager,
          uuid: copyBehaviour == CopyBehaviour.createNew ? null : project.uuid,
          commit:
              copyBehaviour == CopyBehaviour.createNew ? null : project.commit,
          created: copyBehaviour == CopyBehaviour.createNew
              ? DateTime.now()
              : project.created,
          changed: DateTime.now(),
          name: name ?? project.name,
          color: color ?? project.color,
          note: note ?? project.note,
          client_uuid: client_uuid,
        );

  @override
  Map<String, dynamic> toMap() => super.toMap()
    ..addAll(
      {
        'client_uuid': client_uuid,
        'color': color,
        'name': name,
        'note': note,
      },
    );

  @override
  Set<AbstractTimemanagerObject> get dependentChildObjects {
    var children = <AbstractTimemanagerObject>{};
    children.addAll(tasks);
    children.addAll(times);
    return children;
  }
}

extension TimeDuration on Project {
  Duration get duration => tasks.fold<Duration>(
      Duration(), (previousValue, element) => previousValue + element.duration);

  Duration getDurationBetween(DateTime start, DateTime end,
          {List<Task> tasks = const []}) =>
      times
          .where((time) =>
              time.start.isAfter(start) &&
              time.start.isBefore(end) &&
              (tasks.isEmpty || tasks.contains(time.task_uuid)))
          .fold<Duration>(Duration(),
              (previousValue, time) => previousValue + time.duration);
}

extension RelationExtensions on Project {
  Client get client => timemanager.get<Client>(client_uuid)!;

  Set<Task> get tasks => timemanager
      .getAll<Task>()
      .where(
        (task) => task.project_uuid == uuid,
      )
      .toSet();

  Set<Time> get times => tasks.expand((task) => task.times).toSet();
}
